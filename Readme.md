# Timeline 
- Read PDF
- Install Virtualbox, Packer 
- What is 'Packer', how does it work?
- Run basic packer build Ubuntu (fails)
- What is a 'builder', how does it work?
- Retry with server 2016 ISO
- Installer fails - not enough RAM?
- Add RAM, checksum, shutdown command
- Ran install with boot_command
 - boot_command is tedious and difficult
- What is 'autoUnattend.xml', how does it work?
- Attempted to install with AutoUnattend
 - Installation fails - no OS versions
 - Remove License Key to correct this
 - Installation fails - Could not complete this install 
 - Remove RunSynchronous commands
 - Reapplied license key in OOBE pass
- Install works, boots to GUI
- Investigate running code with autounattend
 - Run code to setup winrm
 - Run code to setup RDP
 - Run code to setup Timezone
 - Run code to run windows update
 - Run code to disable WSUS auto update ? (Can't verify...?)
- Script code, add to floppy drive
- Run code from floppy drive via autounattend
- What is a 'provisioner', how does it work?
- Moved code to powershell provisioners
- Build fails at windows update reboot
- Testing using windows update provisioner 
- Added Reboot
- Added code to remove autologon
- Create tests to check the requirements and print it to a file on the desktop
 - Timezone 
 - RDP test 
 - winRM test
 - test firewall is on 
 - test auto updates is disabled
 - print to screen update history
 
 
# Comments
 - Installed environment with chocolatey
   - choco upgrade virtualbox -y
   - choco upgrade packer -y 
   - choco upgrade packer-provisioner-windows-update -y

 - The windows-update provisioner only works if you have the packer-provisioner-windows-update file in the same folder as packer
   - I didn't get to try the chocolatey package, I pulled the file directly from the git page here: https://github.com/rgl/packer-provisioner-windows-update
 - Windows updates take a long time - with more RAM this might speed up

 - Should run on a machine with more RAM, 2GB seems to be the bare minimum of server 2016
 - Would like to take the time to research if scripts or powershell provisioners are best practice
 - Would like to take the time to understand every option in the autounattend.xml 
 - I wasn't able to create any assertion scripts however I would use some of the following
   - Testing timezone: (Get-Timezone).ID -eq 'UTC'
   - Testing RDP: (Test-NetConnection 127.0.0.1 -CommonTCPPort RDP).TcpTestSucceeded -eq $true
   - Testing Disabled updates: (New-ItemProperty HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name NoAutoUpdate).noautoupdate -eq 1
